<?php

use \App\Domains\Lookups\Http\Controllers\API\LocationApiController;
use App\Domains\Lookups\Http\Controllers\API\PageApiController;
use App\Http\Middleware\ForceJsonResponse;
use App\Http\Middleware\ApiLocaleMiddleware;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

//force json response middleware
Route::group(['middleware' => ForceJsonResponse::class], function (){

    //localization group middleware
    Route::group(['middleware' => ApiLocaleMiddleware::class], function (){

        Route::group([
            'prefix' => 'lookups',
            'as' => 'lookups.'
        ], function(){

            Route::get('getCountries', [LocationApiController::class, 'getCountries']);

            Route::get('getCities', [LocationApiController::class, 'getCities']);

            Route::get('getPageBySlug', [PageApiController::class, 'getPageBySlug']);
        });

        //Required Auth token routes
        Route::group(['middleware' => 'auth:sanctum'], function (){


        });

    });
});
