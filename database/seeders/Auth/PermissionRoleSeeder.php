<?php

namespace Database\Seeders\Auth;

use App\Domains\Auth\Models\Permission;
use App\Domains\Auth\Models\Role;
use App\Domains\Auth\Models\User;
use Database\Seeders\Traits\DisableForeignKeys;
use Illuminate\Database\Seeder;

/**
 * Class PermissionRoleTableSeeder.
 */
class PermissionRoleSeeder extends Seeder
{
    use DisableForeignKeys;

    /**
     * Run the database seed.
     */
    public function run()
    {
        $this->disableForeignKeys();

        // Create Roles
        Role::create([
            'id' => 1,
            'type' => User::TYPE_ADMIN,
            'name' => 'Administrator',
        ]);

        // Non Grouped Permissions
        //

        // Grouped permissions
        // Users category
        $users = Permission::create([
            'type' => User::TYPE_ADMIN,
            'name' => 'admin.access.user',
            'description' => 'All User Permissions',
        ]);

        $users->children()->saveMany([
            new Permission([
                'type' => User::TYPE_ADMIN,
                'name' => 'admin.access.user.list',
                'description' => 'View Users',
            ]),
            new Permission([
                'type' => User::TYPE_ADMIN,
                'name' => 'admin.access.user.deactivate',
                'description' => 'Deactivate Users',
                'sort' => 2,
            ]),
            new Permission([
                'type' => User::TYPE_ADMIN,
                'name' => 'admin.access.user.reactivate',
                'description' => 'Reactivate Users',
                'sort' => 3,
            ]),
            new Permission([
                'type' => User::TYPE_ADMIN,
                'name' => 'admin.access.user.clear-session',
                'description' => 'Clear User Sessions',
                'sort' => 4,
            ]),
            new Permission([
                'type' => User::TYPE_ADMIN,
                'name' => 'admin.access.user.impersonate',
                'description' => 'Impersonate Users',
                'sort' => 5,
            ]),
            new Permission([
                'type' => User::TYPE_ADMIN,
                'name' => 'admin.access.user.change-password',
                'description' => 'Change User Passwords',
                'sort' => 6,
            ]),
        ]);

        // Assign Permissions to other Roles
        //

        //Page
        $page = Permission::create([
            'type' => User::TYPE_ADMIN,
            'name' => 'admin.lookups.page',
            'description' => __('All Page Permissions'),
        ]);

        $page->children()->saveMany([
            new Permission([
                'type' => User::TYPE_ADMIN,
                'name' => 'admin.lookups.page.list',
                'description' => __('View Page'),
            ]),
            new Permission([
                'type' => User::TYPE_ADMIN,
                'name' => 'admin.lookups.page.store',
                'description' => __('Create Page'),
                'sort' => 2,
            ]),
            new Permission([
                'type' => User::TYPE_ADMIN,
                'name' => 'admin.lookups.page.update',
                'description' => __('Update Page'),
                'sort' => 3,
            ]),
            new Permission([
                'type' => User::TYPE_ADMIN,
                'name' => 'admin.lookups.page.delete',
                'description' => __('Delete Page'),
                'sort' => 4,
            ]),
        ]);
        //

        //Country
        $countries = Permission::create([
            'type' => User::TYPE_ADMIN,
            'name' => 'admin.lookups.country',
            'description' => __('All Country Permissions'),
        ]);

        $countries->children()->saveMany([
            new Permission([
                'type' => User::TYPE_ADMIN,
                'name' => 'admin.lookups.country.list',
                'description' => __('View Country'),
            ]),
            new Permission([
                'type' => User::TYPE_ADMIN,
                'name' => 'admin.lookups.country.store',
                'description' => __('Create Country'),
                'sort' => 2,
            ]),
            new Permission([
                'type' => User::TYPE_ADMIN,
                'name' => 'admin.lookups.country.update',
                'description' => __('Update Country'),
                'sort' => 3,
            ]),
            new Permission([
                'type' => User::TYPE_ADMIN,
                'name' => 'admin.lookups.country.delete',
                'description' => __('Delete Country'),
                'sort' => 4,
            ]),
        ]);
        //

        //City
        $cities = Permission::create([
            'type' => User::TYPE_ADMIN,
            'name' => 'admin.lookups.city',
            'description' => __('All City Permissions'),
        ]);

        $cities->children()->saveMany([
            new Permission([
                'type' => User::TYPE_ADMIN,
                'name' => 'admin.lookups.city.list',
                'description' => __('View City'),
            ]),
            new Permission([
                'type' => User::TYPE_ADMIN,
                'name' => 'admin.lookups.city.store',
                'description' => __('Create City'),
                'sort' => 2,
            ]),
            new Permission([
                'type' => User::TYPE_ADMIN,
                'name' => 'admin.lookups.city.update',
                'description' => __('Update City'),
                'sort' => 3,
            ]),
            new Permission([
                'type' => User::TYPE_ADMIN,
                'name' => 'admin.lookups.city.delete',
                'description' => __('Delete City'),
                'sort' => 4,
            ]),
        ]);

        $this->enableForeignKeys();
    }
}
