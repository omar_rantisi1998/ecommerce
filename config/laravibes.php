<?php

return [
    /**
     * Lazy loading Prevention logging lottery
     * As we roll this out, we don't want to absolutely smash our
     * Sentry account in the case that there are many, many violations.
     * Instead we will only report based on a lottery.
     *
     * Our default lottery reports every 1 in 500 lazy loads.
     */
    'lazy_loading_reporting_lottery' => array_map(
        'intval',
        explode(',', env('LOG_LAZY_LOADING_DETECTION_LOTTERY', '1,500'))
    ),

    /**
     * APP URL
     */
    'app_url' => env('APP_URL', 'https://vibessolutions.com')
];
