<?php

namespace App\Enums\Core;

use App\Enums\BaseEnum;

final class StoragePaths extends BaseEnum
{

    const USER_PROFILE_PICTURE = 'storage/users/profile_pictures/';
}
