<?php

namespace App\Enums\Core;

use App\Enums\BaseEnum;

final class ErrorTypes extends BaseEnum
{

    const GENERAL = 0;
    const AUTH = 1;
    const USER = 3;
}
