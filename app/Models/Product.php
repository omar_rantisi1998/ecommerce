<?php

namespace App\Models;

use App\Models\BaseModel;

/**
 * @property integer $category_id
 * @property integer $created_by_id
 * @property integer $updated_by_id
 * @property string $name
 * @property string $name_ar
 * @property string $slug
 * @property string $short_description
 * @property string $description
 * @property float $regular_price
 * @property float $sale_price
 * @property string $SKU
 * @property string $stock_status
 * @property boolean $featured
 * @property int $quantity
 * @property string $image
 * @property string $images
 * @property string $created_at
 * @property string $updated_at
 * @property string $deleted_at
 * @property Category $category
 * @property User $user
 * @property User $user
 */
class Product extends BaseModel
{
    /**
     * @var array
     */
    protected $fillable = ['category_id', 'created_by_id', 'updated_by_id', 'name', 'name_ar', 'slug', 'short_description', 'description', 'regular_price', 'sale_price', 'SKU', 'stock_status', 'featured', 'quantity', 'image', 'images', 'created_at', 'updated_at', 'deleted_at'];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function category()
    {
        return $this->belongsTo('App\Models\Category');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function user()
    {
        return $this->belongsTo('App\Models\User', 'updated_by_id');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function user()
    {
        return $this->belongsTo('App\Models\User', 'created_by_id');
    }
}
