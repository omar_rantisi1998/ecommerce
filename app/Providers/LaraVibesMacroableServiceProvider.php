<?php

namespace App\Providers;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\LazyLoadingViolationException;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\ServiceProvider;
use \Validator;

/**
 * LaraVibesMacroableServiceProvider
 * Created By Amer Almoghrabi (Vibes Solutions)
 * @package LaraVibes Framework
 */
class LaraVibesMacroableServiceProvider extends ServiceProvider
{
    /**
     * Register services.
     *
     * @return void
     */
    public function register()
    {
        //
    }

    /**
     * Bootstrap services.
     *
     * @return void
     */
    public function boot()
    {
        // all our deployed environments, including our development, UAT, and
        // SIT environments have APP_ENV set to "production" but we want these
        // environments to report violations so we utilise another environment
        // variable to know which one is *actually* production.
        Model::preventLazyLoading(
            $this->app['config']->get('app.deployed_env') !== 'production'
        );

        // this is our custom handler that gets
        // triggered when lazy loading is detected...
        Model::handleLazyLoadingViolationUsing(function (Model $model, string $relation): void {
            $lottery = $this->app['config']->get('laravibes.lazy_loading_reporting_lottery');

            if (random_int(1, $lottery[1]) <= $lottery[0]) {
                $exception = new LazyLoadingViolationException($model, $relation);

                if ($this->app['config']->get('app.env') === 'local') {
                    throw $exception;
                } else {
                    report($exception);
                }
            }

            // Add a header to the response.
            if (! app()->runningInConsole()) {
                header('Lazy-Loading-Violation: 1');
            }
        });


        //createdBy
        Blueprint::macro('addCreatedBy', function ($column = 'created_by_id', $foreignTable = 'users', $foreignPrimaryId = 'id') {
            $this->unsignedBigInteger($column)->nullable();

            $this->foreign($column)
                ->references($foreignPrimaryId)->on($foreignTable)
                ->onDelete('set null');
        });

        //updatedBy
        Blueprint::macro('addUpdatedBy', function ($column = 'updated_by_id', $foreignTable = 'users', $foreignPrimaryId = 'id') {
            $this->unsignedBigInteger($column)->nullable();

            $this->foreign($column)
                ->references($foreignPrimaryId)->on($foreignTable)
                ->onDelete('set null');
        });

        /**
         * Validates that two or more fields are unique
         */
        Validator::extend('unique_multiple', function ($attribute, $value, $parameters, $validator)
        {
            //if this is for an update then don't validate
            //todo: this might be an issue if we allow people to "update" one of the columns..but currently these are getting set on create only
            if (isset($validator->getData()['id'])) return true;

            // Get table name from first parameter
            $table = array_shift($parameters);
            // Build the query

            $query = DB::table($table);

            // Add the field conditions
            foreach ($parameters as $i => $field){
                $query->where($field, $validator->getData()[$field]);
            }
            // Validation result will be false if any rows match the combination
            return ($query->count() == 0);

        });

        /**
         * Add replacer message for unique_multiple function
         */
        Validator::replacer('unique_multiple', function($message, $attribute, $rule, $parameters) {
            return str_replace(array(':table', ':other'), $parameters, $message);
        });
    }
}
