<?php

namespace App\Providers;

use App\Domains\FirebaseIntegration\FirebaseIntegration;
use App\Services\StorageManagerService;
use Illuminate\Pagination\Paginator;
use Illuminate\Support\ServiceProvider;

/**
 * Class AppServiceProvider.
 */
class AppServiceProvider extends ServiceProvider
{
    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        //Storage manager singleton registration
        $this->app->singleton(StorageManagerService::class,function (){
            return new StorageManagerService();
        });

        //Firebase integration singleton registration
        $this->app->singleton(FirebaseIntegration::class,function (){
            return new FirebaseIntegration();
        });
    }

    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        Paginator::useBootstrap();

    }
}
